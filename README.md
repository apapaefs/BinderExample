# BinderExample

This is a simple example of how to use Binder.

To add dependencies via anaconda, edit the file environment.yml.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/gitlab/repo/master)
